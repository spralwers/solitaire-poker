#ifndef CARD_STACK
#define CARD_STACK

#include "card.h"
#include <list>
#include <iterator>
#include <time.h>

using namespace std;

class CardStack
{
    public:

    Card *cards;
    list<Card> cardsList;

    void shuffle(); //shuffle cards that are in the deck

    bool isEmpty(); // check if empty

    Card pop(); //take the top card off the stack

    void push(Card card); //put a card

    Card peek();

    string ShowDeck();

    int size;

    int getSize();

    list<Card>::iterator iter;

    list<Card>::iterator start() { return cardsList.begin();}
    list<Card>::iterator finish() { return cardsList.end();}

};

class MasterDeck : public CardStack
{

    public:
    void initialize();
    Card peek();


};

class PlayerDeck : public CardStack
{
    public:
    //string showDeck();
    Card* withdraw(int);

};

#endif
