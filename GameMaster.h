#ifndef GAMEMASTER_H
#define GAMEMASTER_H

#include "cardstack.h"

//Macros for calculating scores.
//scores are based on probabilities of getting poker hands.
//we are doing 5 hand poker. Since the order does not matter,
//we are looking for all possible COMBINATIONS of 5 cards
//from a deck of 52. 52 choose 5 = 2,598,960
#define TotalCombinations = 2598960

//Macros for scores
#define ROYAL_FLUSH 5000
#define STRAIGHT_FLUSH 2500
#define FLUSH 1800
#define STRAIGHT 1800
#define FOUR_OF_A_KIND 800
#define FULL_HOUSE 700
#define THREE_OF_A_KIND 500
#define ONE_PAIR 100

//used to make printing poker hands less of a hassle
#define POINTS " points!\n"

class GameMaster
{
    public:
        GameMaster();
        int Getscore() { return score; }
        int runGame();
    //private:
        int score;
        const int playerDeckSize = 5;

        enum ValueCombos
        {
            //10, J, Q, K, A
            Royal_Sequence=10,
            //Any numerical sequence except royal
            //ie 2, 3, 4, 5 or 5, 6, 7, 8, 9
            Normal_Sequence=9,
            //Four cards with the same value, 5th unrelated
            //ie 5, 5, 5, 5, 3
            Four_of_a_Kind = 8,
            //Three same values, two same values
            //ie 3, 3, 3, 8, 8
            Full_House = 7,
            //Three same values, two unrelated values
            //ie 9, 9, 9, J, Q
            Three_of_a_Kind = 6,
            //Two different pairs, each pair has same number
            //ie 7, 7, K, K, 9
            Two_Pairs = 5,
            //Two cards have the same value
            One_Pair = 4,
            //Nothing has the same value, with no sequence.
            //ie 2 4 7 9 A
            No_Matches_No_Sequence = 3,

        };



        enum SuitCombos
        {
            //All cards have the same suit.
            //i.e. Diamonds, Diamonds, Diamonds, Diamonds, Diamonds
            All_The_Same,
            //There are at least two
            At_Least_Two,
        };

        void swap(MasterDeck &, PlayerDeck &);
        int scoreDeck(PlayerDeck &);
        int doubleOrNothing(MasterDeck &, PlayerDeck &);
        bool allSameSuit(PlayerDeck &);
        int valueComboCheck(PlayerDeck &);

};

#endif // GAMEMASTER_H
