#include "GameMaster.h"
#include <iostream>
#include <string>
#include "cardstack.h"
#include <stdio.h>
#include <algorithm>

void lower(string &lowered)
{

    for (unsigned int i=0; i<lowered.length(); i++)
        lowered[i] = tolower(lowered[i]);

        /*
        cout << lowered[1] << endl;
        cout << lowered << endl;*/
}

GameMaster::GameMaster()
{
    score = 0;
}


int GameMaster::runGame()
{
    cout << "Starting the game.";

    //initialize the master deck. Generate the 52 cards, then shuffle it.
    MasterDeck theDeck;
    theDeck.initialize();
    theDeck.shuffle();

    //take 5 cards from the shuffled master deck and give them to the player.
    PlayerDeck playersHand;
    for (int i=0; i<5; i++)
        playersHand.push(theDeck.pop());

    cout << "Here are your cards:" << endl << playersHand.ShowDeck();

    //used to store the choices of the user.
    string input;

    //have the player decide to swap cards or keep their hand.
    do
    {
        cout << "Stay or swap? Type 'stay' to stay, 'swap' to swap." << endl;
        cin >> input;

        //force lower case for easy checking.
        lower(input);

    //and must be used instead of OR, because the compare function actually returns 0 if true!
    //and operator forces the result to be 0.
    }while(input != "stay" && input != "swap");

    //if they want to swap, this is where it happens.
    if (input == "swap")
        swap(theDeck, playersHand);

    //user's deck is now ready to be evaluated.
    score = scoreDeck(playersHand);

    //check if the score is > 0. If it is, that means they got a valid
    //deck, and are ready to play double or nothing. Check if the user
    //wants to play double or nothing.
    if (score >0){

        do{
            cout << "Would you like to play double or nothing? Y/N" << endl;
            cin >> input;
            lower(input);
        }while(input != "y" && input != "n");

        if (input == "y")
            score = doubleOrNothing(theDeck, playersHand);
    }

    //the game is done, return the score.
    cout << "Your score is: " << score << ". Thanks for playing!" << endl;
    return score;

}

//takes any number of cards from the user's deck and swaps them out with
//cards in the master deck.
void GameMaster::swap(MasterDeck &theDeck, PlayerDeck &playersDeck)
{
    cout << "You'll be asked, in order from top to bottom, if you" << endl;
    cout << "want to swap a card. Use Y for yes, any other word/character for no." << endl;

    //put players deck onto array for easy index based swapping
    Card temp[5];
    for (int i=0; i<5; i++)
        temp[i] = playersDeck.pop();

    int count=0;
    string input;
    do
    {
        cout << "Would you like to swap " << temp[count].printCard() << "? y/n" << endl;
        cin >> input;
        lower(input);

        if (input == "y")
        {
            //first put the card to be swapped to the back of the master deck.
            //note that push for master deck pushes to the back, not front.
            theDeck.push(temp[count]);
            temp[count] = theDeck.pop();
            playersDeck.push(temp[count]);
            cout << "swapped for: " << temp[count].printCard() << endl;
        }
        else
        {
            theDeck.push(temp[count]);
            playersDeck.push(temp[count]);
        }

        count++;
    }while (count < 5);

    cout << "your new set of cards:" << endl << playersDeck.ShowDeck() << endl;

}

//evaluates the poker hands. It prioritizes the highest scoring hands.
int GameMaster::scoreDeck(PlayerDeck &playersDeck)
{

    //Get two pieces of information about the deck:
    //If all the cards are the same suit
    //What combination of values there are in the deck,
        //refer to enum type ValueCombos in gamemaster.h
    bool sameSuit = allSameSuit(playersDeck);
    int combo = valueComboCheck(playersDeck);

    //royal flush, straight flush, and flush all depend on
    //all cards being the same suit.
    if (sameSuit == true)
    {
        switch (combo)
        {
            case Royal_Sequence:
                cout << "Royal Flush! " << ROYAL_FLUSH << POINTS;
                return ROYAL_FLUSH;

            case Normal_Sequence:
                cout << "Straight Flush! " << STRAIGHT_FLUSH << POINTS;
                return STRAIGHT_FLUSH;

            default:
                cout << "Flush! " << FLUSH << POINTS;
                return FLUSH;
        }
    }

    //if cards aren't all the same suit, then we're left
    //with straights and below.

    switch (combo)
    {
        case Normal_Sequence:
            cout << "Straight! " << STRAIGHT << POINTS;
            return STRAIGHT;

        case Four_of_a_Kind:
            cout << "Four of a kind! " << FOUR_OF_A_KIND << POINTS;
            return FOUR_OF_A_KIND;

        case Full_House:
            cout << "Full house! " << FULL_HOUSE << POINTS;
            return FULL_HOUSE;

        case Three_of_a_Kind:
            cout << "Three of a kind! " << THREE_OF_A_KIND << POINTS;
            return THREE_OF_A_KIND;
        case One_Pair:
            cout << "A pair! " << ONE_PAIR << POINTS;
            return ONE_PAIR;
        default:
            cout << "Bad hand. Too bad. 0 Points." << endl;
            return 0;
    }


}

int GameMaster::doubleOrNothing(MasterDeck &theDeck, PlayerDeck &playersDeck)
{
    int count=5;
    bool fail = false;
    cout << "Welcome to double or nothing\n";
    int multScore=score;

    //put all the players cards back to the master deck.
    for (int i=0; i<playersDeck.getSize(); i++)
        theDeck.push(playersDeck.pop());

    //The card that the player is shown first.
    Card firstCard;
    //Holds the next card that gets compared with the
    //first card.
    Card nextCard;
    string input="";
    string comparison;
    cout << "Enter high if you think the next card will be higher, low if you think\n";
    cout << "the next card will be lower.\n";
    while ((count < 7) && (fail == false))
    {

        firstCard = theDeck.pop();
        cout << "current card is: " << firstCard.printCard() << "is the next card high or low?\n";
        cin >> input;
        lower(input);
        nextCard = theDeck.pop();
        cout << "Next card is: " << nextCard.printCard() << endl;
        comparison = (nextCard.getValue() > firstCard.getValue()) ? "high" : "low";

        cout << input << " input\n";
        cout << comparison << " comparison\n";

        if (input == comparison)
        {
            cout << input << " is correct! Score went from: " << multScore << " to " << multScore*2 << endl;
            multScore=multScore*2;
            count++;
            if (count == 7)
            {
              cout << "Amazing! You won all rounds of double or nothing!\n";
              break;
            }

        }
        else
        {
            cout << input << " is wrong! Too bad.\n";
            fail = true;
            multScore = 0;
            break;
        }

        cout << "Would you like to continue playing double or nothing? y/n \n";
            cin >> input;
            lower(input);
            if (input == "y")
                fail = false;
            else
                fail = true;

    }

    return multScore;

}

bool GameMaster::allSameSuit(PlayerDeck &playersDeck)
{
    //Cycle through players' deck counting the amount of each suit.
    int hearts=0;
    int diamonds=0;
    int clubs=0;
    int spades=0;

    list<Card>::iterator it;
    string suit = "";
    for (it=playersDeck.start(); it != playersDeck.finish(); it++)
    {
        suit = (*it).getSuit();
        lower(suit);
        if (suit == "hearts")
            hearts++;
        if (suit == "diamonds")
            diamonds++;
        if (suit == "clubs")
            clubs++;
        if (suit == "spades")
            spades++;
    }

    if (hearts == 5 || diamonds == 5 || clubs == 5 || spades == 5)
        return true;

    return false;

}

int GameMaster::valueComboCheck(PlayerDeck &playersDeck)
{
    //Check the combinations of values in order of highest
    //scoring values to lowest.
    //put into array for easy sorting.
    Card sortedList[playerDeckSize];
    list<Card>::iterator it = playersDeck.start();
    for (int i=0; i<playerDeckSize; i++)
    {
        //avoid actually popping cards from the players deck,
        //since the cards won't be replaced.
        sortedList[i] = (*it);
        it++;
    }

    //sort the array in descending order
    std::sort(sortedList, sortedList+playerDeckSize, Card::sortByValue);

    //check if order is sequential.
    //subtraction between each adjacent pair should result in 1.
    //assume first that we have a sequence
    bool sequential = true;
    for (int i=0; i<playerDeckSize-1; i++)
    {
        //check if the adjacent pair is not in sequence.
        //if not, indicate that it is not sequential, and stop checking.
        if (sortedList[i].getValue() - sortedList[i+1].getValue() != 1)
        {
            sequential = false;
            break;
        }

    }

    //but if the deck is in sequence,
    //check first if it's a royal flush. Easy to check, just see if the first
    //card is an Ace, with value 14, since the deck is already sorted.
    if (sequential == true)
    {
        if (sortedList[0].getValue() == 14)
            return Royal_Sequence;
        //otherwise, it's just a regular sequence/
        return Normal_Sequence;
    }

    //now for a flush, we need to make sure there are no matching cards.
    //however, checking if there are no matching cards requires us to
    //check if there are any matching cards. so we're going to look for
    //matching cards first.

    //next, we check how many cards have the same value.
    //the same as checking for duplicates.

    //brute force approach. Make an array that represents
    //all the values, and have the array save the amount
    //of times a number appears.
    //index 0 corresponds to the value 2, 1 to 3,
    //2 to 4, etc. So cards will go to the index of
    //card value - 2.

    //array of size 13, because 2-14 is a range of 13 values.
    //set all values to zero since c++ assigns to random values.

    int valueCount[13] = {0};
    int valueCountSize = 13;

    //go through the cards list, get the value of each card,
    //and increment the corresponding entry in the array to
    //indicate how many times it appears.
    for (int i=0; i<playerDeckSize; i++)
        valueCount[sortedList[i].getValue()-2]++;

    //we go through the valueCount array, to check to what
    //extent there are duplicate numbers.

    //after flushes and straights, four of a kind and full
    //house are the next highest. Check for those.

    //the two bools below are used to see if there are
    //cards that repeat three times (fullHouseThreeCheck)
    //and twice (fullHouseTwoCheck).
    bool fullHouseThreeCheck = false;
    bool fullHouseTwoCheck = false;
    for (int i=0; i<valueCountSize; i++)
    {
        //if we know a card repeats four times,
        //the other has to be different.
        if (valueCount[i] == 4)
            return Four_of_a_Kind;
        if (valueCount[i] == 3)
            fullHouseThreeCheck = true;
        if (valueCount[i] == 2)
            fullHouseTwoCheck = true;
    }

    //if we made it this far, then we know we don't have a
    //four of a kind. see if we have a full house.
    if (fullHouseThreeCheck && fullHouseTwoCheck)
        return Full_House;

    //if we don't have a full house, the next highest score is three
    //of a kind. we already checked if three values are the same,
    //so we can use that.

    if ((fullHouseThreeCheck == true) & (fullHouseTwoCheck == false))
        return Three_of_a_Kind;

    //however, if there are no matching cards,
    //there are no sequences.

    if (fullHouseTwoCheck)
        return One_Pair;

    return No_Matches_No_Sequence;

}
