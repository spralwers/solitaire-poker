#ifndef CARD_H_INCLUDED
#define CARD_H_INCLUDED
#include <string>

using namespace std;
class Card

{

public:

    int value;
    string cardName;
    string suit;

    Card()
    {
        value = 0;
        cardName = "blank";
        suit = "blank";
    }

    Card(int v, string c, string s)
    {
       value = v;
        cardName = c;
        suit = s;
    }

    Card (int v, string s)
    {
        value = v;
        cardName = Card::numToString(v);
        suit = s;
    }

    void setCard(int v, string c, string s)
    {
        value = v;
        cardName = c;
        suit = s;
    }

    string getName()
    {
        return cardName;
    }

    int getValue()
    {
        return value;
    }

    string getSuit()
    {
        return suit;
    }

    string printCard()
    {
        if ((this) == NULL)
            return "";
        return cardName + " of " + suit + '\n';

    }

   static string numToString(int v)
    {

        switch (v)
        {
        case 2:
            return "2";
        case 3:
            return "3";
        case 4:
            return "4";
        case 5:
            return "5";
        case 6:
            return "6";
        case 7:
            return "7";
        case 8:
            return "8";
        case 9:
            return "9";
        case 10:
            return "10";
        case 11:
            return "Jack";
        case 12:
            return "Queen";
        case 13:
            return "King";
        case 14:
            return "Ace";
        }

        return "not valid value";

    }

static bool sortByValue(const Card &c1, const Card &c2)
{
    return (c1.value) > (c2.value);
}

};



#endif // CARD_H_INCLUDED
