#include "cardstack.h"
#include <iterator>
#include <iostream>
#include <stdlib.h>
#include "card.h"

//Function definitions for the general stack of cards.
//=============================================================

//for now, cards will be added to the back (bottom of the deck).

int CardStack::getSize()
{
    return cardsList.size();
}

void CardStack::push(Card card)
{

    cardsList.push_back(card);

}


Card CardStack::pop()
{

   Card cardToReturn = cardsList.front();
   cardsList.pop_front();
   return cardToReturn;

}

bool CardStack::isEmpty()
{
    return cardsList.empty();
}

void CardStack::shuffle()
{

    //fisher-yates algorithm
    srand(time(NULL));

    cout << "seeding" << endl;

    //shuffle an array, faster;
    cards = new Card[52];
    iter = cardsList.begin();

    cout << cardsList.size() << endl;

    cout << "creating card array" << endl;
    for (int i=0; i<52; i++)
    {
     cards[i] = *iter;
     cout << i << endl;
     iter++;
    }
    cout << "created card array" << endl;
    for (int i=51;i>=1;i--)
    {
        int g = rand() % i;
        Card temp = cards[g];
        cards[g] = cards[i];
        cards[i] = temp;

    }
    cout << "sorted cards array" <<endl;
    int i =0;
    for (iter = cardsList.begin(); iter !=cardsList.end(); iter++)
    {
        *iter = cards[i];
        i++;
    }

    cout << "put the sorted cards back" << endl;

    delete cards;

}

string CardStack::ShowDeck()
{
    string listofCards = "";

     for (iter = cardsList.begin(); iter != cardsList.end(); iter++)
     {
         listofCards = listofCards + (*iter).printCard();
     }

        return listofCards;
}

//==============================================================

//Function definitions for the master deck.

//Looks at the front card only
Card MasterDeck::peek()
{
    return cardsList.front();
}

void MasterDeck::initialize()
{
    //use a simple for loop to create a list of cards.

    //First, there are four suits.
    for (int i=0; i<4; i++)
    {
        //next, there are thirteen values.
        for (int j=2; j<=14; j++)
        {

            //switch statement for each suit.
            switch (i)
            {
            case 0: //hearts
                push(Card(j, Card::numToString(j), "Hearts"));
                break;

            case 1: //clubs
                push (Card(j, Card::numToString(j), "Clubs"));
                break;

            case 2: //spades
                push (Card(j, Card::numToString(j), "Spades"));
                break;

            case 3: //diamonds
                push (Card(j, Card::numToString(j), "Diamonds"));
                break;

            }

        }
    }
}

//===============================================================

//Function definitions for players deck.

//Returns the entire deck via iterator

/*string PlayerDeck::showDeck()
{
     string listofCards = "";

     for (iter = cardsList.begin(); iter != cardsList.end(); iter++)
     {
         listofCards = listofCards + (*iter).printCard();
     }

        return listofCards;
}*/

Card* PlayerDeck::withdraw(int index)
{
    Card *withdrawCard;
        if (index >= getSize() || index <= 0)
        {
        std::cout << "Not a valid entry, pick numbers 1 through 5.";
        return NULL;
        }


        else
        {
            iter = cardsList.begin();
            //advance iterator using the advance function in the std lib
            std::advance(iter, index-1);
            //save the address of the withdrawn card
            withdrawCard = &(*iter);
            return withdrawCard;

        }

}
